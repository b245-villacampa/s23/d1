console.log("Send Star!");

// [SECTION] OBJECTS
	// An object is a data type that is used to represent world objects.
	// It is a collection of related data and/or functionalities or method.
	// information stored in objects are represented in "key:value" pair
	//usually we call as property
	// different data type may be stored in an object's property creating data structures.

	// Creating objects using object initializer/object literal notation.

	
		/*
			let/const objectName = {
				keyA: valueA,
				keyB: valueB,
				...
			}

		*/

		// this creates/declares an object and also initializes/assign its properties upon creation.

		let cellphone = {
			name:"Nokia 3210",
			manufactureDate: 1999,
			features:["send message", "make call", "play games"],
			isWorking:true,
			owner:{
				fullName : "Chris",
				yearOfOwnage:20
			}
		}

		console.log(cellphone);

	//Accessing/reading properties
		//Dot notation
			/*
				Syntax:
				objectName.proprtyName;

			*/ 
		console.log(cellphone.name);
		console.log(cellphone.isWorking);
		console.log(cellphone.features);
		console.log(cellphone.features[1]);
		console.log(cellphone.owner);
		console.log(cellphone.owner.fullName);
		// bracket Notation
		console.log("accessing using bracket notation")
		console.log(cellphone["name"]);
		console.log(cellphone["owner"]["fullName"]);

	// Creating objects using constructor function

		/*
			-creates a reausable function to crteate several objects taht have the same data structure.
			-This is useful for creating multiplet instances/copies of an object
			-instance is a concrate occurence of any object which emphasize distinct/unique identity.



				Syntax:
					function objectName(valueA,valueB){
							this.propertyA = valueA,
							this.propertyB = valueB
						}
					
		*/
			//This is a constructor function
				// this keyword allows us to assign object properties by associating them withg the values received from a constructor function's parameter.

				function Laptop(name, manufactureDate,ram,isWorking){
					this.laptopName=name,
					this.laptopNameDate=manufactureDate
					this.laptopRam = ram,
					this.isWorking = isWorking
				}
			
			// Instantiation
				// The NEW keyword create an instance of an object
				let laptop = new Laptop("Lenovo", 2008, "2gb", true);
				console.log(laptop);

				let myLaptop = new Laptop("Macbook Air", 2020, "8gb", false);
				console.log(myLaptop);

				let oldLaptop = new Laptop("Portal R2E CCMC", 1980, "500mb", false);
				console.log(oldLaptop);

				function Menu(mealName, mealPrice){
					this.menuName = mealName,
					this.menuPrice = mealPrice
				}

				let mealOne = new Menu("breakfast", 299);
				console.log(mealOne);

				let mealTwo = new Menu("dinner", 500);
				console.log(mealTwo);

				// create empty objects
				let computer = {};
				let emptyObject = new Object()
				console.log(computer);
				console.log(emptyObject);


				// Access objects inside and array
				let array = [laptop, myLaptop];
				console.log(array);

				//Get the value of the proprty laptopName in the first element of our name

				console.log(array[0].laptopName);
				console.log(array[1].laptopManufactureDate);

			// Initialization/adding/deleting/reassigning Obeject propoerties

				/*
					Like any oither variable in JS, objects have their properties initialized or added after the object was created / declared
				*/

				let car = {}
				console.log(car)

				
				// 	Initialize/add object property using do not notation
					/*
						Syntax:
						object.propertToBeAdded=value;
						
					*/

				car.name = "Honda Civic";
				console.log(car);

				//Initialize/ add object using bracker notation
					/*
						syntax:
						object["propertyToBeAdded"]="value"
					*/

				car["manufactureDate"] = 2019;
				console.log(car);

				//Deleting Object Properties
					//Deleting using dot notation
					/*
						Syntax: delete objectName.propetyToBeDeleted;
					*/ 
				delete car.name;
				console.log(car);

					//Deleting using bracket notation
					/*
						Syntax: delete objectName["propetyToBeDeleted"]";
					*/ 
				delete car["manufactureDate"];
				console.log(car);

				//Reassigning object properties
				car.name="Honda Civi";
				car["manufactureDate"]=2019;
				console.log(car);

				//Reassigning object property using dot notation 
					car.name = "Dodge Charger R/T";
					console.log(car);

				//Reassigning object property using brcaket notation 
					car["name"] = "Jeepney";
					console.log(car);

					// [Reminder] If you will add a new property to the object make sure that the property name that you are going to add is not existing or else it will just reassign the value of the property.
			
			// Object Methods
					// A method is a function which is a property of an object
					// They are also functions and one of the keydifferences the have is that methods are functions related to a specific object.

				let person ={
					name : "John Doe",
					talk : function(){
						console.log("Hello my name is "+this.name)
					}
				}	

				console.log(person);
				person.talk();

			// add method to object 
				person.walk = function(){
					console.log(person.name+" walked 25 steps forward.")
				}
				console.log(person);
				person.walk();

			// Constructor function with method 

				function Pokemon(name, level){
					// properties of the object
					this.pokemonName = name;
					this.pokemonLevel = level;
					this.pokemonHealth = 2*level;
					this.pokemonAttack = level;

					// Method
					this.tackle = function(targetPokemon){
						console.log(this.pokemonName+" tackles "+targetPokemon.pokemonName);
						let hpAftertackle = targetPokemon.pokemonHealth - this.pokemonAttack;
						console.log(targetPokemon+" health is now reduced to "+hpAftertackle);
					}
					this.faint = function(){
						console.log(this.pokemonName+" fainted!")
					}
				}

				let pikachu = new Pokemon("Pikachu", 12);
				console.log(pikachu);
				let gyarados = new Pokemon("gyarados", 20);
				console.log(gyarados);

				pikachu.tackle(gyarados);
				gyarados.tackle(pikachu);
				pikachu.faint();
